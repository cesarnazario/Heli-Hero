using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SnakeBoss : MonoBehaviour
{
    [SerializeField]
    private float _attackforce;
    public static event Action OnHitMiner;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Miner>())
        {
            var parent = other.gameObject.transform.root;
            if (parent.GetComponent<MinerHook>())
            {
                OnHitMiner?.Invoke();
                var rb = parent.GetComponent<Rigidbody2D>();
                rb.AddForce(Vector2.up * _attackforce);
            }
        }
        if (other.GetComponent<PlayerMovement>())
        {
            var rb = other.GetComponent<Rigidbody2D>();
            rb.AddForce(Vector2.up * _attackforce, ForceMode2D.Impulse);
        }
    }
}
