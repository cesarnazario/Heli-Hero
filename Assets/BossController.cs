using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public GameObject _bossHead;
    public GameObject _bossBody;
    [SerializeField]
    private Transform _playerPos;

    [SerializeField]
    private float _idleTime = 10f;


    private Animator _bossHeadAnim;
    private Animator _bossBodyAnim;
    private bool _isCharging;
    private bool _isAttacking;
    private bool _isIdeling;

    private void Start()
    {
        _bossHeadAnim = _bossHead.GetComponentInChildren<Animator>();
        _bossBodyAnim = _bossBody.GetComponentInChildren<Animator>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            if (GameManager.CanControlPlayer && _bossHead != null)
            {
                _bossHead.SetActive(true);
                StartCoroutine(ChargingAttack(_idleTime));
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            if (GameManager.CanControlPlayer && _bossHead != null)
            {
                _bossHead.SetActive(false);
                _bossBody.SetActive(false);
            }
        }
    }


    IEnumerator ChargingAttack(float time)
    {
        yield return new WaitForSeconds(time);
        if (_bossHead != null)
        {
            _bossHeadAnim.SetBool("Charging", true);
        }
    }
    public void Attack()
    {


        _bossHead.SetActive(false);
        _bossBody.SetActive(true);
        _bossBody.transform.position = _playerPos.position;

    }

    public void Idel()
    {
        Debug.Log("Body attack");
        _bossBody.SetActive(false);
        _bossHead.SetActive(true);
        _bossHeadAnim.SetBool("Charging", false);
        StartCoroutine(ChargingAttack(_idleTime));

    }

}
