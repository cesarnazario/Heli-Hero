using System.Collections;
using UnityEngine;
using System;
using UnityEngine.Experimental.Rendering.Universal;
using Cinemachine;

public class Boom : MonoBehaviour
{

    private Collider2D[] _inExplosionRadius = null;
    [SerializeField]
    private float _forceMuilt = 5f;
    [SerializeField]
    private float _radius = 5f;
    [SerializeField]
    private float _explodeTime = 1.5f;
    [SerializeField]
    private AudioSource _boom;
    [SerializeField]
    private GameObject _explosion;
    [SerializeField]
    private Light2D _lightGO;

    private float _timer = 0f;
    private Light2D _light;
    private CinemachineVirtualCamera _cam;
    private GameObject _pc;

    public static event Action OnHitPlayer;
    private void Start()
    {
        _cam = GameObject.FindGameObjectWithTag("CM").GetComponent<CinemachineVirtualCamera>();
        _light = _lightGO.GetComponent<Light2D>();
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        gameObject.GetComponent<Animator>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;

    }

    private void FixedUpdate()
    {
        BoomTimer();

    }

    private void Explode()
    {

        _inExplosionRadius = Physics2D.OverlapCircleAll(transform.position, _radius);
        foreach (Collider2D item in _inExplosionRadius)
        {
            if (item.GetComponent<Wall>() || item.GetComponent<Floor>())
            {

                Destroy(item.gameObject);

            }
            Rigidbody2D itemRb = item.GetComponent<Rigidbody2D>();
            if (itemRb != null && !item.GetComponent<Boom>())
            {
                Vector2 distance = (item.transform.position - transform.position) / 2f;
                if (item.GetComponent<PlayerMovement>())
                {
                    GameManager.CanControlPlayer = false;
                    OnHitPlayer?.Invoke();
                    Debug.Log("Playerdead");
                }
                if (distance.magnitude > 0 && !item.GetComponent<Miner>())
                {
                    float explosionForce = _forceMuilt / distance.magnitude;
                    itemRb.AddForce(distance.normalized * explosionForce, ForceMode2D.Impulse);
                    itemRb.constraints = RigidbodyConstraints2D.None;
                }
                if (distance.magnitude > 0 && item.GetComponent<Miner>())
                {
                    float explosionForce = _forceMuilt / distance.magnitude;
                    itemRb.AddForce(distance.normalized * explosionForce * 0.1f, ForceMode2D.Impulse);
                    itemRb.constraints = RigidbodyConstraints2D.None;
                }
                if (item.GetComponent<Core>())
                {
                    Destroy(item.transform.root.gameObject);
                }
            }
        }
        if (_boom.isPlaying == false)
        {
            _boom.PlayOneShot(_boom.clip);
            _cam.GetComponent<CinemachineImpulseSource>().GenerateImpulse();
            _pc = Instantiate(_explosion, gameObject.transform.position, gameObject.transform.rotation);
        }
        CleanBoom();
    }

    private void BoomTimer()
    {
        _timer += Time.fixedDeltaTime;
        _light.intensity = Mathf.Lerp(0.1f, 0.5f, _timer);
        if (_timer > _explodeTime)
        {
            Explode();
            _timer -= _explodeTime * 10;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, _radius);
    }

    IEnumerator DeleteBoom(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
        Destroy(_pc);
    }
    private void CleanBoom()
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        gameObject.GetComponent<Animator>().enabled = false;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        StartCoroutine(DeleteBoom(3f));
    }
}
