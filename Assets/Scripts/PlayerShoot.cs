﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerShoot : MonoBehaviour
{

    [Header("Gun")]
    //Bullet speed
    [SerializeField]
    private float _bulletForce = 400f;
    //How long time delay to do reload 
    [SerializeField]
    private float _fireRate = 10f;
    [SerializeField]
    private GameObject _bulletPrefab;
    [SerializeField]
    private Transform _bulletSpawnPoint;
    [SerializeField]
    private ParticleSystem _muzzleFlash;
    [SerializeField]
    private AudioSource _shootAudioSource;

    [Header("Boom")]
    [SerializeField]
    private GameObject _boom;
    [SerializeField]
    private Vector3 _boomOffset;




    //Used for fire rate
    private float _lastFired;
    private bool _isShooting;

    //AUtomatic fire
    public void Shoot(InputAction.CallbackContext context)
    {
        if (context.performed && GameManager.CanControlPlayer)
        {
            _isShooting = true;
        }
        else
        {
            _isShooting = false;
        }
    }
    public void Boom(InputAction.CallbackContext context)
    {
        if (context.performed && GameManager.CanControlPlayer && UIManager.IsPaused == false)
        {
            StartCoroutine(Boom(0f));
        }
    }

    private IEnumerator Boom(float time)
    {
        yield return new WaitForSeconds(time);


        Instantiate(_boom, transform.position + PlayerMovement.inputVector.x * _boomOffset, transform.rotation);

    }

    private void Shooting()
    {
        if (_isShooting)
        {
            if (Time.time - _lastFired > 1 / _fireRate)
            {
                _lastFired = Time.time;

                //Shoot once, muzzleFlash once 
                _muzzleFlash.Emit(1);

                //Shooting sound play
                _shootAudioSource.PlayOneShot(_shootAudioSource.clip);

                //Instantiate bullet from bulletSpawnPoint
                GameObject bullet = Instantiate(_bulletPrefab, _bulletSpawnPoint.transform.position, _bulletSpawnPoint.transform.rotation);

                //Add velocity to the bullet 
                bullet.GetComponent<Rigidbody2D>().velocity = -bullet.transform.right * _bulletForce;
            }
        }
    }

    void FixedUpdate()
    {
        Shooting();
    }
}
