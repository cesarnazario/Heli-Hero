using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightingController : MonoBehaviour
{
    [SerializeField]
    private Animator _globalLightAnim;


    [SerializeField]
    private float _lightTime = 20f;
    [SerializeField]
    private float _darkTime = 10f;
    [SerializeField]
    private bool _canChagneLight = true;


    private void Start()
    {
        if (_canChagneLight)
        {
            StartCoroutine(TurnOffLight(_lightTime));
        }
    }

    IEnumerator TurnOffLight(float lightTime)
    {
        yield return new WaitForSeconds(lightTime);
        if (GameManager.CanControlPlayer)
        {
            _globalLightAnim.SetTrigger("ChangeState");
            StartCoroutine(TurnOnLight(_darkTime));
        }

    }

    IEnumerator TurnOnLight(float darkTime)
    {
        yield return new WaitForSeconds(darkTime);
        if (GameManager.CanControlPlayer)
        {
            _globalLightAnim.SetTrigger("ChangeState");
            StartCoroutine(TurnOffLight(_lightTime));
        }
    }
}
