using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour
{
    [SerializeField]
    private AudioClip _winBGM;
    [SerializeField]
    private AudioClip _gameBGM;
    private AudioSource _audioBGM;

    private void Awake()
    {
        _audioBGM = GetComponent<AudioSource>();
        _audioBGM.clip = _gameBGM;
    }

    private void Start()
    {

       // _audioBGM.PlayOneShot(_audioBGM.clip);

    }
    private void OnEnable()
    {
        Goal.OnGameWin += PlayWinSound;

    }
    private void OnDisable()
    {
        Goal.OnGameWin -= PlayWinSound;
    }

    private void PlayWinSound()
    {
        _audioBGM.clip = _winBGM;
        _audioBGM.PlayOneShot(_audioBGM.clip);
    }

}
