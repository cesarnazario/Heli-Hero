using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using System;

public class UIManager : MonoBehaviour
{

    [SerializeField]
    private AudioMixer _mixer;
    [SerializeField]
    private GameObject _pauseScreen;
    [SerializeField]
    private GameObject _gameOverScreen;
    [SerializeField]
    private GameObject _WinScreen;
    [SerializeField]
    private GameObject _HUDScreen;
    private TMP_Text _scoreDisplay;
    [SerializeField]
    private Image[] _livesDisplay;
    [SerializeField]
    private Image[] _minerDisplay;
    private Color _fullAlpha = new Color(1, 1, 1, 1);
    private Color _halfAlpha = new Color(1, 1, 1, .5f);
    private int _livesRemain;
    private int _minerRemain;
    //private TMP_Text _livesDisplay;
    //private TMP_Text _minerDisplay;
    public static event Action OnCleanDataAndPos;
    public static event Action OnCleanScore;

    public static bool IsPaused;
    private bool _isMute;
    private float _volume;

    private void Awake()
    {
        _livesRemain = 3;
        _minerRemain = 0;
    }
    private void OnEnable()
    {
        Goal.OnGameWin += DelayLevelCompleted;
        GameManager.OnGameOver += GameOver;
    }
    private void OnDisable()
    {
        Goal.OnGameWin -= DelayLevelCompleted;
        GameManager.OnGameOver -= GameOver;
    }

    public void Start()
    {
        if (_HUDScreen != null)
        {

            _scoreDisplay = _HUDScreen.GetComponentInChildren<TMP_Text>();
            var imageList = _HUDScreen.GetComponentsInChildren<Image>();

            _livesDisplay[0] = imageList[0];
            _livesDisplay[1] = imageList[1];
            _livesDisplay[2] = imageList[2];
            _minerDisplay[0] = imageList[3];
            _minerDisplay[1] = imageList[4];
            _minerDisplay[2] = imageList[5];

            foreach (var item in _livesDisplay)
            {
                item.color = _fullAlpha;
                //print(item.gameObject.name);
            }

            foreach (var item in _minerDisplay)
            {
                item.color = _halfAlpha;
                //print(item.gameObject.name);

            }
        }
    }
    private void Update()
    {
        if (_scoreDisplay != null)
        {
            _scoreDisplay.text = string.Format("{000}", GameManager.Score.ToString());
            if (_livesRemain != GameManager.LivesCount && GameManager.LivesCount != 0)
            {
                if (_livesRemain > 0)
                {
                    _livesDisplay[_livesRemain - 1].color = _halfAlpha;
                    _livesRemain--;

                }
            }
            if (_minerRemain != GameManager.MinerCount)
            {
                _minerDisplay[_minerRemain].color = _fullAlpha;
                _minerRemain++;
            }
        }
        // print($"Lives count;  {GameManager.LivesCount}");
        //print($"Lives remainig;  {_livesRemain}");
        // print($"Miners count;  {GameManager.MinerCount}");
        // print($"Miner remainig;  {_minerRemain}");

    }

    public void SetLevel(float sliderValue)
    {
        _mixer.SetFloat("MusicVol", Mathf.Log10(sliderValue) * 20);
    }
    public void SetFXLevel(float sliderValue)
    {
        _mixer.SetFloat("FXVol", Mathf.Log10(sliderValue) * 20);
    }

    public void Mute()
    {

        _isMute = !_isMute;
        _volume = _isMute ? -80f : 0;
        _mixer.SetFloat("MusicVol", _volume);
    }

    public void PlayGame(string sceneName)
    {
        if (_gameOverScreen != null)
        {
            _gameOverScreen.SetActive(false);
            Time.timeScale = 1;
        }
        SceneManager.LoadScene(sceneName);
    }
    public void LosingRetry()
    {
        TurnScreenOff(_gameOverScreen);
        GameManager.LivesCount = 3;
        Time.timeScale = 1;
    }

    public void BackToMain()
    {
        OnCleanDataAndPos?.Invoke();
        OnCleanScore?.Invoke();
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");

    }
    public void Resume()
    {
        Time.timeScale = 1;

    }
    public void TurnScreenOn(GameObject Screen)
    {
        Screen.SetActive(true);
        EventSystem.current.SetSelectedGameObject(Screen.GetComponentInChildren<Button>().gameObject);
    }

    public void TurnScreenOff(GameObject Screen)
    {

        Screen.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);

    }
    public void ExitGameButton()
    {

        Application.Quit();
    }

    public void Pause()
    {
        IsPaused = !IsPaused;
        _pauseScreen.SetActive(IsPaused);
        Time.timeScale = !IsPaused ? 1 : 0;
        if (IsPaused)
        {
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(_pauseScreen.GetComponentInChildren<Button>().gameObject);
        }
        //print("pause");
    }
    public void GameOver()
    {
        OnCleanDataAndPos?.Invoke();
        _gameOverScreen.SetActive(true);
        Time.timeScale = 0;
        //var text = _gameOverScreen.GetComponentInChildren<TMP_Text>();
        //text.text = string.Format("Score: {0}", GameManager.Score.ToString());
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(_gameOverScreen.GetComponentInChildren<Button>().gameObject);
    }
    public void DelayLevelCompleted()
    {
        Invoke("LevelCompleted", 2f);
    }
    private void LevelCompleted()
    {

        _WinScreen.SetActive(true);
        Time.timeScale = 0;
        var text = _WinScreen.GetComponentInChildren<TMP_Text>();
        text.text = string.Format("{0}", GameManager.Score.ToString());
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(_WinScreen.GetComponentInChildren<Button>().gameObject);
        OnCleanDataAndPos?.Invoke();
    }
}
