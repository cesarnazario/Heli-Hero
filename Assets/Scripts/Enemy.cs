using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction { Horizontal, Vertical, Static };

public class Enemy : MonoBehaviour
{
    private Vector3 _endPosX;
    private Vector3 _endPosY;
    private PlayerMovement _player;
    public Direction EnemyDirection;
    private float _timePassed;
    private Vector3 _originPos;
    public float Offset;
    public float Speed;
    [SerializeField]
    private AudioSource _deadSFX;
    public static event Action OnPlayerDie;


    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<PlayerMovement>();
        _originPos = transform.position;
        _endPosX = new Vector3(transform.localPosition.x + Offset, transform.localPosition.y, 0);
        _endPosY = new Vector3(transform.localPosition.x, transform.localPosition.y - Offset, 0);

    }

    // Update is called once per frame
    void Update()
    {
        _timePassed += Time.deltaTime;
        switch (EnemyDirection)
        {
            case Direction.Horizontal:
                transform.position = Vector3.Lerp(_originPos, _endPosX,
                    Mathf.PingPong(_timePassed, 1));

                break;
            case Direction.Vertical:
                transform.position = Vector3.Lerp(_originPos, _endPosY,
                    Mathf.PingPong(_timePassed, 1));

                break;
            case Direction.Static:
                var sprite = gameObject.GetComponent<SpriteRenderer>().sprite.bounds.size;
                var col = GetComponent<BoxCollider2D>();
                col.size = sprite;
                col.offset = new Vector2((sprite.x / 4), 0);
                break;
        }


    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        var bullet = other.collider.GetComponent<Bullet>();

        if (bullet != null)
        {
            Instantiate(_deadSFX, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            GameManager.CanControlPlayer = false;
            OnPlayerDie?.Invoke();
        }

    }

}
