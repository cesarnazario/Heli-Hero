using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MinerHook : MonoBehaviour
{


    [SerializeField]
    private int _hookId;
    [SerializeField]
    private PlayerMovement _player;
    [SerializeField]
    private MinerIdData _minerData;
    [SerializeField]
    private MinerIdData _minerSavedData;
    [SerializeField]
    private GameObject _helpAnim;
    [SerializeField]
    private GameObject _saveAnim;
    [SerializeField]
    private AudioSource _hitSound;

    private DistanceJoint2D[] _distanceJoints;
    private Collider2D _col;
    public static event Action OnMinerPickUp;
    private void Awake()
    {

        CheckPoint.Gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        if (_minerData.MinerId.Any())
        {
            for (int i = _minerData.MinerId.Count - 1; i >= 0; i--)
            {
                if (_minerData.MinerId[i] == _hookId)
                {
                    transform.position = CheckPoint.Gm.lastCheckPointPos;
                }
            }
        }
    }


    private void Start()
    {
        _distanceJoints = _player.GetComponents<DistanceJoint2D>();
        _col = GetComponent<BoxCollider2D>();
    }
    private void Update()
    {
        AnimationCheck();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerMovement>())
        {

            for (int i = 0; i < _distanceJoints.Length; i++)
            {
                if (i == _hookId)
                {
                    if (!_minerData.MinerId.Contains(_hookId))
                    {
                        _minerData.MinerId.Add(_hookId);
                        if (!_minerSavedData.MinerId.Contains(_hookId))
                        {
                            _minerSavedData.MinerId.Add(_hookId);
                        }
                        GameManager.MinerCount = _minerData.MinerId.Count;
                        print($"pickup {other.name}");
                        OnMinerPickUp?.Invoke();
                    }
                    _distanceJoints[i].enabled = true;
                    _col.enabled = false;
                }
            }
        }
    }

    private void OnEnable()
    {
        Goal.OnGameWin += GetJointNumber;
        SnakeBoss.OnHitMiner += HitMiner;
    }
    private void OnDisable()
    {
        Goal.OnGameWin -= GetJointNumber;
        SnakeBoss.OnHitMiner -= HitMiner;
    }

    public void GetJointNumber()
    {
        if (_minerData.MinerId.Contains(_hookId))
        {
            var hingeJoints = this.GetComponentsInChildren<HingeJoint2D>();
            var fixedJoints = this.GetComponentsInChildren<FixedJoint2D>();
            var jointsCount = hingeJoints.Length + fixedJoints.Length;
            GameManager.JointNumber += jointsCount;
        }
    }
    public void HitMiner()
    {
        for (int i = 0; i < _distanceJoints.Length; i++)
        {
            if (i == _hookId)
            {
                if (_minerData.MinerId.Contains(_hookId))
                {
                    _minerData.MinerId.Remove(_hookId);
                    GameManager.MinerCount = _minerData.MinerId.Count;
                    Debug.Log(GameManager.MinerCount);

                }
                _distanceJoints[i].enabled = false;
                _col.enabled = true;
                _hitSound.PlayOneShot(_hitSound.clip);
            }
        }
    }

    private void AnimationCheck()
    {
        if (_distanceJoints[_hookId].enabled == false)
        {
            _saveAnim.gameObject.SetActive(false);
            _helpAnim.gameObject.SetActive(true);
        }
        else if (_distanceJoints[_hookId].enabled == true)
        {
            _saveAnim.gameObject.SetActive(true);
            _helpAnim.gameObject.SetActive(false);
        }
    }

}
