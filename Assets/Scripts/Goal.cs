using UnityEngine;
using System;

public class Goal : MonoBehaviour
{
    public static event Action OnGameWin;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            if (GameManager.CanControlPlayer)
            {
                OnGameWin?.Invoke();
                this.GetComponent<BoxCollider2D>().enabled = false;
            }

        }
    }
}
