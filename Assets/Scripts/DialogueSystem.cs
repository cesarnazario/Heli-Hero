using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;

public class DialogueSystem : MonoBehaviour
{
    [SerializeField]
    private string[] _messages;
    [SerializeField]
    private string[] _names;
    [SerializeField]
    private GameObject _dialogueBox;
    private TMP_Text _nameDisplay;
    private TMP_Text _messageDisplay;
    private TMP_Text _pressToContinue;
    private bool _next;

    private int turn = 0;

    private void OnEnable()
    {
        MinerHook.OnMinerPickUp += DisplayMessage;
    }
    private void OnDisable()
    {
        MinerHook.OnMinerPickUp -= DisplayMessage;
        //MinerHook.OnMinerPickUp -= //StartCoroutine
    }
    private void Awake()
    {
        var textList = _dialogueBox.GetComponentsInChildren<TMP_Text>();
        _nameDisplay = textList[0];
        _messageDisplay = textList[1];
        _pressToContinue = textList[2];
        _pressToContinue.gameObject.SetActive(false);

    }
    private void DisplayMessage()
    {
        Time.timeScale = 0;
        StartCoroutine(Dialogue());

        //print("event call");

    }

    public void Next(InputAction.CallbackContext context)
    {
        if (context.performed && GameManager.CanControlPlayer)
        {
            _next = true;
        }
        else
        {
            _next = false;
        }
    }

    IEnumerator Dialogue()
    {

        //update
        while (turn < _messages.Length)
        {
            print("start coroutine");
            _pressToContinue.gameObject.SetActive(false);
            _next = false;
            // GameManager.CanControlPlayer = false;
            _dialogueBox.SetActive(true);
            _nameDisplay.text = _names[turn];
            _messageDisplay.text = _messages[turn];
            turn++;
            _pressToContinue.gameObject.SetActive(true);
            yield return new WaitUntil(() => _next);
        }

        GameManager.CanControlPlayer = true;
        _dialogueBox.SetActive(false);
        Time.timeScale = 1.0f;
        StopAllCoroutines();
    }

}
