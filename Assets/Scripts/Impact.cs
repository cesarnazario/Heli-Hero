﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Impact : MonoBehaviour
{
    //How long before the impact is destroyed
    [SerializeField]
    private float _spawnTimer = 2.0f;
    [SerializeField]
    private AudioSource _audioSource;

    private void Start()
    {
        //Play the impact sound
        _audioSource.PlayOneShot(_audioSource.clip);
        // Start the spawn timer
        Destroy(gameObject, _spawnTimer);
    }

}
