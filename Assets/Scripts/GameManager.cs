using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static bool CanControlPlayer;
    public static int JointNumber;
    public static int Score;
    public static int MinerCount;
    public static int LivesCount = 3;
    public static int MinersLenght;

    [SerializeField]
    private float _delayLoadTime;
    [SerializeField]
    private MinerIdData _minerData;
    [SerializeField]
    private MinerIdData _minerSavedData;
    [SerializeField]
    private List<GameObject> _miners;
    [SerializeField]
    private int _minerPartScore = 1000;
    private float _delayTimer;


    private static GameManager instance;
    public Vector2 lastCheckPointPos;
    public static event Action OnGameOver;

    void Awake()
    {
        CanControlPlayer = true;
        _delayTimer = _delayLoadTime;
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }

        var objs = FindObjectsOfType<MinerHook>();
        MinersLenght = objs.Length;

    }

    private void OnEnable()
    {
        Goal.OnGameWin += CaculateScore;
        Boom.OnHitPlayer += LoadSceneDelay;
        Wall.OnHitPlayer += LoadSceneDelay;
        Enemy.OnPlayerDie += LoadSceneDelay;
        UIManager.OnCleanDataAndPos += ClearPosAndData;
        UIManager.OnCleanScore += ClearPlayerScore;

    }
    private void OnDisable()
    {
        Goal.OnGameWin -= CaculateScore;
        Boom.OnHitPlayer -= LoadSceneDelay;
        Wall.OnHitPlayer -= LoadSceneDelay;
        Enemy.OnPlayerDie -= LoadSceneDelay;
        UIManager.OnCleanDataAndPos -= ClearPosAndData;
        UIManager.OnCleanScore -= ClearPlayerScore;

    }

    public void ClearHookData()
    {
        _minerData.MinerId.Clear();
        _minerSavedData.MinerId.Clear();
    }

    public void CaculateScore()
    {
        StartCoroutine(GetScore(0.1f));
        MinerCount = _minerData.MinerId.Count;
        LivesCount = 3;
    }
    IEnumerator GetScore(float time)
    {
        yield return new WaitForSeconds(time);
        Score = JointNumber * _minerPartScore;
        Debug.Log("score" + Score);
        ClearHookData();
    }

    public void LoadSceneDelay()
    {
        if (CanControlPlayer == false)
        {
            LivesCount--;
            StartCoroutine(LoadScene(2f));
        }
    }
    IEnumerator LoadScene(float time)
    {
        yield return new WaitForSeconds(time);
        for (int i = 0; i < _minerSavedData.MinerId.Count; i++)
        {
            if (!_minerData.MinerId.Contains(i))
            {
                _minerData.MinerId.Add(i);
            }
        }
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        GameManager.CanControlPlayer = true;
        MinerCount = _minerData.MinerId.Count;
    }
    private void Update()
    {
        if (LivesCount < 1)
        {
            OnGameOver?.Invoke();
            LivesCount = 3;
        }

    }
    public void ClearPlayerPos()
    {
        lastCheckPointPos = Vector2.zero;
    }
    public void ClearPlayerScore()
    {
        Score = 0;
    }

    public void ClearPosAndData()
    {
        ClearPlayerPos();
        ClearHookData();
    }


}
