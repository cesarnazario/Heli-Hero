using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [Header("PlayerSettings")]
    [SerializeField]
    private float _upForce = 1000f;
    [SerializeField]
    private float _moveForce = 500f;
    [SerializeField]
    private ForceMode2D _forceMode = ForceMode2D.Force;
    [SerializeField]
    private ParticleSystem _landParticles;
    [SerializeField]
    private AudioSource _landAudio;
    [SerializeField]
    private AudioSource _deadAudio;

    [Header("FanSettings")]
    [SerializeField]
    private Transform _fan;
    [SerializeField]
    private float _maxFanSpeed = 4000f;
    [SerializeField]
    private AudioSource _fanAudio;
    [SerializeField]
    private ParticleSystem _fanParticles;


    [SerializeField]
    private LayerMask _groundLayer;


    public bool IsLanded;

    public static Vector2 inputVector;

    private Rigidbody2D _rigidBody;
    private float _upInput;
    private float _moveInput;
    private float _currentFanSpeed;
    private float _fanVol;
    private float _fanPitch;
    private bool _isPlayFanSound = false;
    private CircleCollider2D _col;
    private Animator _anim;
    private bool _facingRight = true;


    // Start is called before the first frame update
    void Start()
    {
        CheckPoint.Gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        transform.position = CheckPoint.Gm.lastCheckPointPos + new Vector2(0f, 0.5f);
        _rigidBody = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
        _col = GetComponent<CircleCollider2D>();
        _fanVol = 0f;
        _fanPitch = 0f;
    }

    void Update()
    {
        FanPlay(_isPlayFanSound);
        CleanPlayerStates();
    }

    void FixedUpdate()
    {

        ApplyRotation();
        ApplyForce();
        ApplyFanRotation();

    }


    public void Move(InputAction.CallbackContext context)
    {
        inputVector = context.ReadValue<Vector2>();
        SetInputVector(inputVector);
    }

    private void SetInputVector(Vector2 inputVector)
    {
        if (GameManager.CanControlPlayer)
        {
            _upInput = inputVector.y;
            _moveInput = inputVector.x;
        }
    }
    void ApplyForce()
    {
        Vector2 forceVector = transform.up * _upInput;
        if (_upInput > 0f && GameManager.CanControlPlayer)
        {
            _rigidBody.AddForce(forceVector * _upForce * Time.fixedDeltaTime, _forceMode);
            _currentFanSpeed = Mathf.Lerp(_currentFanSpeed, _maxFanSpeed, Time.fixedDeltaTime);
            _isPlayFanSound = true;
        }
        else
        {
            _currentFanSpeed = Mathf.Lerp(_currentFanSpeed, 0f, Time.fixedDeltaTime);
            _isPlayFanSound = false;
        }

        Vector2 rotateVector = Vector2.right * _moveInput;
        _rigidBody.AddForce(rotateVector * _moveForce * Time.fixedDeltaTime, _forceMode);
    }

    void ApplyRotation()
    {
        if (_moveInput < 0 && !_facingRight)
        {
            Flip();
        }
        else if (_moveInput > 0 && _facingRight)
        {
            Flip();
        }
    }
    void ApplyFanRotation()
    {
        _fan.Rotate(0f, 0f, _currentFanSpeed * Time.fixedDeltaTime);
    }
    private void FanPlay(bool canplay)
    {

        if (!IsGrounded())
        {
            IsLanded = false;
        }
        _fanAudio.volume = _fanVol;
        _fanAudio.pitch = _fanPitch;
        if (IsGrounded() && IsLanded == false)
        {
            IsLanded = true;
            _anim.SetTrigger("Landing");
            _landAudio.PlayOneShot(_landAudio.clip);
            _landParticles.Play();
        }
        if (canplay)
        {
            _fanVol = Mathf.Lerp(_fanVol, 1f, Time.deltaTime * 2f);
            _fanPitch = Mathf.Lerp(_fanPitch, 2f, Time.deltaTime * 2f);
            var emission = _fanParticles.emission;
            emission.rateOverTime = 80;
            _anim.SetBool("IsFly", true);
        }
        else
        {
            _fanVol = Mathf.Lerp(_fanVol, 0f, Time.deltaTime * 2f);
            _fanPitch = Mathf.Lerp(_fanPitch, 0f, Time.deltaTime * 2f);
            var emission = _fanParticles.emission;
            emission.rateOverTime = 0;
            _anim.SetBool("IsFly", false);
        }
    }

    private void Flip()
    {
        _facingRight = !_facingRight;
        transform.Rotate(0f, 180f, 0f);

    }
    private bool IsGrounded()
    {
        float extraHeifht = 0.1f;
        var raycastHit = Physics2D.BoxCast(_col.bounds.center, _col.bounds.size, 0f, Vector2.down, extraHeifht, _groundLayer);
        return raycastHit.collider != null;
    }
    private void CleanPlayerStates()
    {
        if (GameManager.CanControlPlayer == false)
        {
            _isPlayFanSound = false;
            gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
            _fanVol = 0;
            _fanPitch = 0;
            _anim.SetBool("IsFly", false);
        }
    }
    private void OnEnable()
    {
        Boom.OnHitPlayer += PlayDeadEffects;
        Wall.OnHitPlayer += PlayDeadEffects;
        Enemy.OnPlayerDie += PlayDeadEffects;
    }


    private void OnDisable()
    {
        Boom.OnHitPlayer -= PlayDeadEffects;
        Wall.OnHitPlayer -= PlayDeadEffects;
        Enemy.OnPlayerDie -= PlayDeadEffects;
    }

    private void PlayDeadEffects()
    {
        _deadAudio.PlayOneShot(_deadAudio.clip);
        _anim.SetTrigger("Dead");
    }
}
