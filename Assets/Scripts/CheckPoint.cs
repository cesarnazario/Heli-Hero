using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public static GameManager Gm;

    [SerializeField]
    private AudioSource _sound;
    [SerializeField]
    private GameObject _fireFX;
    [SerializeField]
    private Animator _checkAnim;

    private Vector2 _pos;
    private void Start()
    {
        Gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        _pos = new Vector2(transform.position.x, transform.position.y);
        _fireFX.gameObject.SetActive(false);


    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            if (GameManager.CanControlPlayer)
            {
                _fireFX.gameObject.SetActive(true);
                _checkAnim.SetTrigger("Awake");
                if (Gm.lastCheckPointPos != _pos)
                {
                    Gm.lastCheckPointPos = transform.position;
                    _sound.PlayOneShot(_sound.clip);
                    Debug.Log("Checked");
                }

            }

        }
    }
}
