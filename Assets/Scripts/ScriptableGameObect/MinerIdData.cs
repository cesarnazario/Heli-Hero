using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Stats/MinerIdData")]
public class MinerIdData : ScriptableObject
{
    public List<int> MinerId ;
}
