﻿using UnityEngine;
using Cinemachine;

public class Bullet : MonoBehaviour
{
    // After how long time should the bullet prefab be destroyed
    [SerializeField]
    private float _bulletLifeSpawn = 3f;

    [Header("ImpactPrefabs")]
    [SerializeField]
    private Transform _impactPrefabs;

    private CinemachineVirtualCamera _cam;

    private bool _destroyOnImpact;

    private void Awake()
    {
        _cam = GameObject.FindGameObjectWithTag("CM").GetComponent<CinemachineVirtualCamera>();
        _destroyOnImpact = false;

        //If destroy on impact is false, destroy bullet after few time 
        if (_destroyOnImpact == false)
        {

            Destroy(gameObject, _bulletLifeSpawn);
        }
        //Otherwise, destroy bullet on impact
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //If bullet collides with "Blood" tag
        if (!other.gameObject.GetComponent<BossHead>())
        {
            _cam.GetComponent<CinemachineImpulseSource>().GenerateImpulse();

            //Instantiate random impact prefab from array
            Instantiate(_impactPrefabs, transform.position, Quaternion.LookRotation(other.contacts[0].normal));
            //Destroy bullet object
            Destroy(gameObject);

        }

    }
}

