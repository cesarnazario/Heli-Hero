using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private float _offset;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private GameObject _spiderPrefab;
    private GameObject _spiderClone;



    // Start is called before the first frame update
    void Start()
    {
        _spiderClone = Instantiate(_spiderPrefab, transform.position, Quaternion.identity);
        var enemy = _spiderClone.GetComponent<Enemy>();
        enemy.Speed = _speed;
        enemy.Offset = _offset;
    }


}
