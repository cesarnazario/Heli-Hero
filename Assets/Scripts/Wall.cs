using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Wall : MonoBehaviour
{
    public static event Action OnHitPlayer;


    private void OnCollisionEnter2D(Collision2D other)
    {
        //If bullet collides with "Blood" tag
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            GameManager.CanControlPlayer = false;
            OnHitPlayer?.Invoke();
        }
    }
}
